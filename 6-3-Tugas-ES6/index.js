console.log("Shofari Bagus")
console.log("");
//Soal 1
console.log("Soal 1");
const luas = (panjang, lebar) => {
    let l = panjang * lebar;
    return `Luas persegi panjang dengan panjang ${panjang} dan lebar ${lebar} adalah : ${l}`;
}
const keliling = (panjang, lebar) => {
    let l = 2 * panjang + 2 * lebar;
    return `Keliling persegi panjang dengan panjang ${panjang} dan lebar ${lebar} adalah : ${l}`;
}
console.log(luas(5,7));
console.log(keliling(5,7));

console.log('');
//Soal 2
console.log("Soal 2");

const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => { console.log(`${firstName} ${lastName}`)}
      }
}

newFunction("William", "Imoh").fullName()

console.log('');
//Soal 3
console.log("Soal 3");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };
const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby);

console.log('');
//Soal 4
console.log("Soal 4");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


console.log('');
//Soal 5
console.log("Soal 5");
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} ` ;
console.log(before);