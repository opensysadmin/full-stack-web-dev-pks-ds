<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Post extends Model
{
    protected $fillable = ['title','description','user_id'];
    protected $keyType = 'string';
    public $incrementing = false;

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(
            function($model){
                if(empty($model->{$model->getKeyName()})){
                    $model->{$model->getKeyName()} = Str::uuid();
                }
            }
        );
        static::deleting(function($post) {
            $post->comment()->delete();
        });
    }


}
