<?php

namespace App\Listeners;

use App\Mail\OTPRegenerateMail;
use App\Events\OtpRegeneratedEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResendOtpMail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpRegeneratedEvent  $event
     * @return void
     */
    public function handle(OtpRegeneratedEvent $event)
    {
        Mail::to($event->user->email)->send(new OTPRegenerateMail($event->otpcode));

    }
}
