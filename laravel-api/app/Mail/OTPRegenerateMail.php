<?php

namespace App\Mail;

use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OTPRegenerateMail extends Mailable
{
    use Queueable, SerializesModels;
    public $otpcode;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpcode)
    {
        $this->otpcode = $otpcode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.user.otp_regenerated_mail')
        ->subject('Laravel API Full Stack Kode OTP');
    }
}
