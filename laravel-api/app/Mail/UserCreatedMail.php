<?php

namespace App\Mail;
use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreatedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $otpcode;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpcode)
    {
        $this->otpcode = $otpcode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.user.user_created_mail')
        ->subject('Laravel API Full Stack Kode OTP');    }
}
