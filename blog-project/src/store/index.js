import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import counter from './counter'
import alert from './alert'
import dialog from './dialog'
import auth from './auth'

const vuexPersist = new VuexPersist({
    key: 'pkgdigischool',
    storage: localStorage
});

Vue.use(Vuex)

export default new Vuex.Store({
    plugins : [vuexPersist.plugin],
    modules : {
        counter,
        alert,
        dialog,
        auth
    }
})