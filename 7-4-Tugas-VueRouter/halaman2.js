export const halaman2Component = {
    data(){
        return {
            genres : [
                {
                    id : '0',
                    genre : 'Fiksi'
                },
                {
                    id : '1',
                    genre : 'Keluarga'
                },
                {
                    id : '2',
                    genre : 'Romantis'
                }
            ]
        }
    },
    template : 
        `<div>
        <h4>List Genre</h4>
        <ul>
            <li v-for="genre in genres">
                <router-link :to="'/genre/'+genre.id">
                    {{genre.genre}}
                </router-link>
            </li>
        </ul>
        </div>`
    
}