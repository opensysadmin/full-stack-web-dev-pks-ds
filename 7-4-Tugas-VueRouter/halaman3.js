export const halaman3Component = {
    data(){
        return {
            users : [
                {
                    id : '0',
                    username : 'shofari'
                },
                {
                    id : '1',
                    username : 'bagus'
                },
            ]
        }
    },
    template : 
        `<div>
        <h4>List User</h4>
        <ul>
            <li v-for="user in users">
                <router-link :to="'/user/'+user.id">
                    {{user.username}}
                </router-link>
            </li>
        </ul>
        </div>`
    
}