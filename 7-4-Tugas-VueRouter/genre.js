export const genreComponent = {
    data(){
        return {
            genres : [
                {
                    id : 0,
                    genre : 'Fiksi',
                    deskripsi : 'Bercerita tentang sesuatu yang tidak nyata'
                },
                {
                    id : 1,
                    genre : 'Keluarga',
                    deskripsi : 'Bercerita tentang kisah keluarga'
                },
                {
                    id : 2,
                    genre : 'Romantis',
                    deskripsi : 'Bercerita tentang kisah romantis'
                }
            ]
        }
    },
    computed : {
        genre() {
            return this.genres.filter((genre)=>{
                return genre.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template : `
        <div>
            <h4>{{ genre.genre }}</h4>
            <ul>
                <li v-for="(value, num) of genre">
                {{ num+" : "+value}}
                </li>
            </ul>
        </div>
    `

}