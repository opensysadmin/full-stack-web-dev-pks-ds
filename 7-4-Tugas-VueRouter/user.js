export const userComponent = {
    data(){
        return {
            users : [
                {
                    id : 0,
                    username : 'shofari',
                    alamat : 'cimahi'
                },
                {
                    id : 1,
                    username : 'bagus',
                    alamat : 'bandung barat'
                },
               
            ]
        }
    },
    computed : {
        user() {
            return this.users.filter((user)=>{
                return user.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template : `
        <div>
            <h4>{{ user.username }}</h4>
            <ul>
                <li v-for="(value, num) of user">
                {{ num+" : "+value}}
                </li>
            </ul>
        </div>
    `

}