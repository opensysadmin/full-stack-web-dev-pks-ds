var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise no
var i = 0;
const reading = (time) => {
    if(time > 0){
        readBooksPromise(time, books[i])
        .then(reading)
        .catch(error => console.log(error))
        i++;
    }
   else{
       console.log('Waktu saya habis');
   }
}

reading(10000)