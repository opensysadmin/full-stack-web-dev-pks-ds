<?php
//Shofari Bagus//

abstract class hewan{
    public $nama;
    public $darah;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        echo $this->nama." sedang ".$this->keahlian;
    }
}

abstract class fight extends hewan {
    public $attackPower;
    public $defensePower;

    public function serang($lawan){
        $lawan->darah = $lawan->darah - $this->attackPower / $lawan->defensePower;
        echo $this->nama . " sedang menyerang ".$lawan->nama;
        echo "<br>";
        $lawan->diserang();

    }
    public function diserang(){
        echo $this->nama . " sedang diserang<br>";
    }
}

class harimau extends fight {
    public function __construct(){
        $this->nama = "Harimau";
        $this->darah = 50;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defensePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis : ".$this->nama."<br>";
        echo "Darah : ".$this->darah."<br>";
        echo "Jumlah Kaki : ".$this->jumlahKaki."<br>";
        echo "Keahlian : ".$this->keahlian."<br>";
        echo "Attack Power : ".$this->attackPower."<br>";
        echo "Defense Power : ".$this->defensePower."<br>";
    }
}

class elang extends fight {
    public function __construct(){
        $this->nama = "elang";
        $this->darah = 50;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defensePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis : ".$this->nama."<br>";
        echo "Darah : ".$this->darah."<br>";
        echo "Jumlah Kaki : ".$this->jumlahKaki."<br>";
        echo "Keahlian : ".$this->keahlian."<br>";
        echo "Attack Power : ".$this->attackPower."<br>";
        echo "Defense Power : ".$this->defensePower."<br>";
    }
}

$harimau = new harimau;
$elang = new elang;
echo "<h2>Game berbasis OOP </h2>";


echo "<h3>Serangan Pertama </h3>";
$harimau->serang($elang);
echo "<br>";
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();

echo "<h3>Serangan Kedua </h3>";
$elang->serang($harimau);
echo "<br>";
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();

?>
