var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.log("Soal No 1")
for(var i = 0; i<daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}
console.log("")


//soal no 2

function introduce(object) {

    return "Nama saya " + object.name + ", umur saya " + object.age + " tahun, alamat saya di " + object.address + ", dan saya punya hobby yaitu "+ object.hobby;
  }
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log("Soal No 2")
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
console.log("")

//Soal No 3

function hitung_huruf_vokal(string) {
    var tampung = string.toLowerCase();
    var data = tampung.split("");
    var counter = 0;
    for(var i = 0; i < data.length; i++){
        if(data[i] == 'a' || data[i] == 'i' || data[i] == 'u' || data[i] == 'e' ||  data[i] == 'o' ){
            counter++;
        }
    }
    return counter;
  }

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log("Soal No 3")
console.log(hitung_1 , hitung_2) // 3 2


//Soal no 4


function hitung(angka){
    return angka * 2 -2 ;
}
console.log("")
console.log("Soal No 4")
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8